#!/bin/bash

function request_and_check_for {
    expected_status=$1
    res=`curl -silent -I $ENDPOINT | head -n 1|cut -d$' ' -f2`
    if [[ $res != $expected_status ]]; then
        echo "Didn't get a $expected_status as expected"
    fi
}


