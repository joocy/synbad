#!/bin/bash

. test_functions.sh

ENDPOINT=localhost:9090/500_after_3

for req in 1 2 3
do
    request_and_check_for 200
done

request_and_check_for 500

echo "Done!"
